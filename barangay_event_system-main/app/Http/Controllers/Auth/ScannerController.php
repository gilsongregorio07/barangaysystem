<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Models\CitizenCertificate;
use App\Models\User;
use Illuminate\Http\Request;

class ScannerController extends Controller
{
    public function index()
    {
        return view('auth.scanner');
    }

    public function find_qr_code(Request $request)
    {
        $find_qr_code = CitizenCertificate::with('certificates')->where('qr_codes', $request->qr_code)->first();

        if($find_qr_code)
        {
            return response()->json(['find_qr_code'=> $find_qr_code,'status'=> 200]);
        }
        return response()->json(['message'=> 'INVALID Cerficate','status'=> 404]);
    }
}

