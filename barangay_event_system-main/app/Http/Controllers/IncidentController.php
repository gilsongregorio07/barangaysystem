<?php

namespace App\Http\Controllers;

use App\Models\Incident;
use App\Models\IncidentType;
use Illuminate\Http\Request;

class IncidentController extends Controller
{
    public function index()
    {
        $incidents = Incident::all();

        
        foreach($incidents as $incident){
            $incident->people = unserialize($incident->people_involve);
        }
      
        $incident_types = IncidentType::all();

        return view('captain.incidents', compact('incidents', 'incident_types'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'date_incident' => 'required',
            'time_incident' => 'required',
            'location' => 'required',
            'type_incident' => 'required',
            'people_involve' => 'required',
            'details_incident' => 'required',
        ]);
        //dd($request);

        $incident = new Incident();
        $incident->date_incident = $request->date_incident;
        $incident->time_incident = $request->time_incident;
        $incident->location = $request->location;
        $incident->type_incident = $request->type_incident;
        $incident->people_involve = serialize($request->people_involve);
        $incident->details_incident = $request->details_incident;
        $incident->save();
        //Incident::create($request->all());

        return back()->with(['success' => 'Incident Created Successfully']);
    }


    public function update(Request $request)
    {
        $request->validate([
            'date_incident' => 'required',
            'time_incident' => 'required',
            'location' => 'required',
            'type_incident' => 'required',
            'people_involve' => 'required',
            'details_incident' => 'required',
        ]);

        $incident = Incident::find($request->incident_id);

        $incident->date_incident = $request->date_incident;
        $incident->time_incident = $request->time_incident;
        $incident->location = $request->location;
        $incident->type_incident = $request->type_incident;
        $incident->people_involve = serialize($request->people_involve);
        $incident->details_incident = $request->details_incident;
        $incident->save();

        return back()->with(['success' => 'Incident Updated Successfully']);
    }
}
